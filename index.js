/**
 * Created by dmitry on 24.03.2017.
 */
'use strict';

const hidenseek = require('./hidenseek');

const pokemons = [{"name":"Ivysaur", "level":2},
    {"name":"Charmander", "level": 4},
    {"name":"Venusaur", "level": 5},
    {"name":"Bulbasaur", "level": 12}
];

class Pokemon {
    constructor(name, level) {
        this.name = name;
        this.level = level;
    }

    show() {
        console.log('Name: %s, level: %d', this.name, this.level);
    }
}

class PokemonList extends Array {
    constructor(...pokemons) {
        super(...pokemons);
    }
    add(name, level){
        this.push(new Pokemon(name, level));
    }
    show() {
        let count = 0;
        this.forEach(item => {
            item.show();
            count++;
        });
        console.log('Pokemons in list: ' + count);
    }
}

const objects = pokemons.map(
    obj => new Pokemon(obj.name, obj.level)
);

var lost = new PokemonList(...objects);

hidenseek.hide('./field', lost, (err, count) => {
    if (err) {
        console.log(err);
    } else {
        console.log('%d pokemons are hidden!', count);

        let fs = require('fs');
        fs.readdir('./field', (err, files) => {
            if (err) {
                console.log(err);
            } else {
                files.forEach((item) => {
                    hidenseek.seek('./field/' + item, (err, data) => {
                        if (err) {
                            if (err.code === 'ENOENT') {
                                console.log('In directory %s pokemons aren\'t found', item);
                            } else {
                                console.log(err);
                            }
                        } else {
                            var found = new PokemonList();
                            if (Array.isArray(data)) {
                                data.forEach(pokemon => {
                                    let name, level;
                                    [name, level] = pokemon.split('|');
                                    found.add(name, level);
                                });
                            }
                            console.log('There are pokemons in directory %s:', item);
                            found.show();
                            console.log('----')
                        }
                    });
                });
            }
        });
    }
});


