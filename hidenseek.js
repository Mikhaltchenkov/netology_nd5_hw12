/**
 * Created by dmitry on 24.03.2017.
 */
'use strict';

const MAX_POKEMONS_HIDE = 3;
const MAX_FOLDERS = 10;
const fs = require('fs');

function random(max) {
    return Math.floor(Math.random() * max);
}

function getDirName(number) {
    return (number < 10) ? ('0' +  number) : number;
}

function createDir(name) {
    return new Promise((created, error) => {
        fs.mkdir(name, (err) => {
           if (err && err.code !== 'EEXIST') {
               error(err);
           } else {
               created();
           }
        });
    });
}

function writePokemon(file, pokemon) {
    return new Promise((saved, error) => {
       fs.appendFile(file,pokemon.name + '|' + pokemon.level + '\n', (err) => {
           if (err) {
               error(err);
           } else {
               saved();
           }
       });
    });
}

function hide(path, list, cb) {
    var count = (list.length > MAX_POKEMONS_HIDE) ? MAX_POKEMONS_HIDE : list.length;
    var promises = [];
    var pokemonsHide = Array(list.length).fill(1);
    pokemonsHide.forEach(function(item, i , arr) {
        arr[i] = random(list.length);
    });
    var folders = Array(count).fill(1);
    folders.forEach(function(item, i , arr) {
        arr[i] = random(MAX_FOLDERS) + 1;
    });
    for (let i = 1; i <= MAX_FOLDERS; i++) {
        promises.push(createDir(path + '/' + getDirName(i)));
    }
    Promise.all(promises)
        .then(() => {
            promises = [];
            for (let i = 0; i < count; i++) {
                promises.push(writePokemon(path + '/' + getDirName(folders[i]) + '/pokemon.txt', list[pokemonsHide[i]]));
            }
            Promise.all(promises).then(cb(null, count));
        })
        .catch((error) => cb(error));
}

function seek(path, cb) {
    fs.readFile(path + '/pokemon.txt', 'utf-8', (err, data) => {
        if (err) {
            return cb(err);
        }
        cb(null, data.split('\n').reduce((array, item) =>
            (item == '')?array:array.concat(item),[]));
    });
}

module.exports = {
    hide,
    seek
};